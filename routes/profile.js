const express = require('express');
const router = express.Router();
const STATUS = require('../config/status');
const mongoose = require('mongoose');

const User = require('../models/User');
const Post = require('../models/Post');

router.get('/:id', (req, res) => {
  if (
    req.user && 
    Object.keys(req.user).length && 
    req.user._id == req.params.id
  ) {
    User.findById(req.params.id)
      .then(user => {
        res.render('profile/edit', {
          user
        })
      })
      .catch(err => console.log(err))
  } else {
    User.findById(req.params.id)
    .then(user => {
      Post.find({
          user: user.id
        })
        .then(posts => {
          res.render('profile/user', {            
            findUser: user,
            posts
          })
        })
        .catch(err => console.log(err));
    })
    .catch(err => console.log(err));
  }  
});

module.exports = router;