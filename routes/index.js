const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Post = require('../models/Post');

router.get('/', (req, res) => {  
  res.render('main/index');
});

module.exports = router;