const express = require('express');
const router = express.Router();
const fs = require('fs');
const mongoose = require('mongoose');
const STATUS = require('../config/status');
const keys = require('../config/keys');
const bcrypt = require('bcryptjs');
const User = require('../models/User');
const passport = require('passport');
const validationLogin = require('../validation/login');
const validationRegister = require('../validation/registration');

router.get('/register', (req, res) => {
  res.render('user/register');
})

router.get('/login', (req, res) => {  
  res.render('user/login');
})

router.post('/register', (req, res) => {
  const state = validationRegister(req.body);
  const errors = state.errors;
  const emailField = req.body.email;
  
  if(!state.isValid) {
    const {password, email, firstName, lastName} = errors;
    return res.render('user/register', {
      password, email, firstName, lastName
    });
  }

  if(req.user) {
    res.redirect('/');
  }
  User.findOne({
      email: req.body.email
    })
    .then(user => {
      if (user) {
        req.flash("error_msg", "Эта почта уже занята");        
        return res.redirect('/users/register');
      } else {
        const newUser = new User({
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          email: req.body.email,
          password: req.body.password
        });

        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            newUser.password = hash;
            newUser
              .save()
              .then(user => res.redirect('/users/login'))
              .catch(err => console.log(err));
          });
        });
      }
    })
});

router.post('/login', (req, res, next) => { 
  const state = validationLogin(req.body);
  
  if(!state.isValid) {
    req.flash('error_msg', 'Email или пароль введены неправильно');    
  }
  
  passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/users/login'
  })(req, res, next)
});

router.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/');
});

module.exports = router;