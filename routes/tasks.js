const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Task = require('../models/Task');

router.get('/', (req, res) => {
  Task.find()
    .then(tasks => {
      res.render('tasks/index', {
        tasks
      });
    })

});

router.get('/show/:id', (req, res) => {
  Task.findById(req.params.id)
    .then(task => res.render('tasks/show', {
      task
    }))
});

router.get('/create', (req, res) => {
  res.render('tasks/create');
});

router.post('/create', (req, res) => {
  const newTask = {
    title: req.body.title,
    body: req.body.body,
    level: req.body.level,
    shortView: req.body.shortView
  };

  new Task(newTask)
    .save()
    .then(task => res.redirect(`tasks/show/${task.id}`));
});

module.exports = router;