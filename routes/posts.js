const express = require('express');
const router = express.Router();
const STATUS = require('../config/status');
const mongoose = require('mongoose');
const {ensureAuthenticated} = require('../helpers/auth');

const Post = require('../models/Post');
const User = require('../models/User');

router.get('/', (req, res) => {      
  Post
    .find({}, {lang: true, title: true, date: true })        
    .sort({date: -1})
    .then(posts => {
      let content = [];
      const info = posts.map(it => content.push({title: it.title, date: it.date, id: it._id}));
      const langs = posts.map(it => it.lang).filter((it, i, arr) => arr.indexOf(it) === i);          

      res.render('posts/index', {content, langs})
    })
});

router.get('/create', ensureAuthenticated, (req, res) => {
  res.render('posts/create');
});

router.post('/create', ensureAuthenticated, (req, res) => {
  const newPost = {
    user: req.user.id,
    title: req.body.title,
    lang: req.body.lang,
    body: req.body.body
  };

  new Post(newPost)
    .save()
    .then(post => {
      res.redirect(`/posts/show/${post.id}`)
    })
});

router.get('/show/:id', (req, res) => {
  Post
    .findById(req.params.id)
    .then(post =>  { 
      User.findById(post.user)
        .then(user => {
          res.render('posts/show', {post, fullName: `${user.firstName} ${user.lastName}`, userID: user.id})
        })
    });
});

router.get('/edit/:id', (req, res) => {
  Post
    .findById(req.params.id)
    .then(post => {
      res.render('posts/edit', {post})
    })
    .catch(err => res.send('Такого поста нет'));
});

router.put('/:id', (req, res) => {
  Post
    .findOne({_id: req.params.id})
    .then(post => {
      post.title = req.body.title;
      post.preview = req.body.preview;
      post.level = req.body.level;
      post.lang = req.body.lang;
      post.description = req.body.description;
      post
        .save()
        .then(() => res.redirect(`/posts/show/${req.params.id}`));
    })
});

router.post('/find', (req, res) => {
  const title = req.body.title;
  Post.find({
      body: {
        $regex: new RegExp(title, 'gi')
      }
    },
    {
      lang: true,
      title: true
    })
    .then((posts) => {
      let content = [];
      const info = posts.map(it => content.push({title: it.title, date: it.date, id: it._id}));
      const langs = posts.map(it => it.lang).filter((it, i, arr) => arr.indexOf(it) === i);          

      res.render('posts/index', {content, langs})
    })
    .catch(err => {
      res.json({error: err})
    }) 
});

router.get('/:category_name', (req, res) => {
  Post.find({}, {lang: true, title: true, date: true})
    .then(posts => {
      let content = [];
      const info = posts.map(it => it.lang === req.params.category_name && content.push({title: it.title, date: it.date, id: it._id}));
      const langs = posts.map(it => it.lang).filter((it, i, arr) => arr.indexOf(it) === i);          

      res.render('posts/index', {content, langs})  
    })
})

module.exports = router;