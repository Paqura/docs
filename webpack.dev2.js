const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const uglifyJs = require('uglifyjs-webpack-plugin');

module.exports = merge(common, {
  mode: 'development',
  devtool: "inline-source-map",
  plugins: [
    new uglifyJs()
  ]
})