const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostShema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  title: {
    type: String,
    required: true
  }, 
  lang: {
    type: String, // HTML, CSS, JS, RUST
    required: true
  },
  body: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Post = mongoose.model('posts', PostShema);