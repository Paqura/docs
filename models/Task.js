const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TaskShema = new Schema({
  title: {
    type: String,
    required: true
  },
  shortView: {
    type: String,
    required: true
  },  
  body: {
    type: String,
    required: true
  },
  level: {
    type: String,
    required: true
  },
  date: {
    type: String,
    default: Date.now
  }
})

module.exports = Task = mongoose.model('tasks', TaskShema);