const moment = require('moment');
const {format} = require("moment/locale/ru");

module.exports = {  
  parseBodyPost: function(input) {    
    const json = JSON.parse(input); 
    return json;
  },
  formatDate: function(date, format) {
    return moment(date, "YYYYMMDD").fromNow();
  }
}