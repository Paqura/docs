const path = require('path');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  entry: "./public/js/client.ts",
  output: {
    filename: "client.js",
    path: path.resolve(__dirname, './public/js')
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: { 
            loader : 'style-loader', options: {sourceMap: true}
          },
          use: [
            {
              loader: 'css-loader',
              options: {              
                minimize: true,
                sourceMap: true
              }
            },         
            { loader: 'sass-loader', options: {sourceMap: true}},
            'postcss-loader'
            ]
        })
      },
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        exclude: /node_modules/
      },
      {
        test: /\.glsl$/,
        loader: 'webpack-glsl-loader'
      }
    ]
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"],
    plugins: [
      new TsconfigPathsPlugin({
        configFile: "tsconfig.json"
      })
    ]
  },
  plugins: [
    new ExtractTextPlugin('../css/bundle.css'),
    new CleanWebpackPlugin('public/js/client.js'),
  ]
};