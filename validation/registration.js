const validator = require('validator');

module.exports =  ({ password, email, firstName }) => {
  const state = {
    isValid: true,
    errors: {}
  };

  if(!firstName.length) {
    state.errors.firstName = 'А как же имя, имярек?';
    state.isValid = false;
  }
  
  if(password.length < 4) {
    state.errors.password = 'Пароль слишком короткий';
    state.isValid = false;
  }

  if(!validator.isEmail(email)) {
    state.errors.email = 'Что-то не так, вот пример: example@gmail.com';
    state.isValid = false;
  }
  

  return state;
};