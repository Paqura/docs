const validator = require('validator');

module.exports =  ({ password, email }) => {
  const state = {
    isValid: true,
    errors: {}
  };
  
  if(password.length < 4) {
    state.errors.password = 'Пароль слишком короткий';
    state.isValid = false;
  }

  if(!validator.isEmail(email)) {
    state.errors.email = 'Неверный формат email: example@gmail.com';
    state.isValid = false;
  }
  

  return state;
};