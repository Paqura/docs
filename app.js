const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const methodOverride = require('method-override');
const bodyParser = require('body-parser');
const keys = require('./config/keys');
const exphbs = require('express-handlebars');
const session = require("express-session");
const MongoStore = require('connect-mongo')(session);
const passport = require('passport');
const flash = require("connect-flash");

const app = express();
const http = require('http').Server(app);
const socket = require('socket.io')(http);
const port = process.env.PORT || 8080;

/** Chat */

socket.on('connection', (socket) => {    
  socket.on('sendMessage', (msg) => {
    socket.broadcast.emit('sendMessage', msg, false);
    socket.emit('sendMessage', msg, true);
  });
  socket.on('disconnect', () => {
    console.log('Что-то по отключение');
  });          
});


/**
 * * Кеширование 
 */

const NodeCache = require( "node-cache" );
const myCache = new NodeCache({ stdTTL: 100, checkperiod: 120 });


/**
 * * Импорт роутов
 */

const index = require('./routes/index');
const posts = require('./routes/posts');
const tasks = require('./routes/tasks');
const users = require('./routes/users');
const profile = require('./routes/profile');

/**
 * * Passport middleware
 */

app.use(passport.initialize());

// * passport config

require('./config/passport')(passport);

/**
 * * session middleware
 */

const mongoOpts = {
  url: keys.mongoURI,
  autoRemove: 'interval',
  autoRemoveInterval: 10 // In minutes. Default
} 

app.use(
  session({
    secret: "secret",
    resave: true,
    saveUninitialized: true,
    store: new MongoStore(mongoOpts)
  })
);

/**
 * * Паспорт мидлвары, обязательно после сессии экспресса
 */

app.use(passport.initialize());
app.use(passport.session());

/**
 * * Подключение Mongoose
 */

mongoose.connect(keys.mongoURI)
  .then(() => console.log('mongo connected'))
  .catch((err) => console.error(err));

/**
 * * Глобальные промимы
 */

mongoose.Promise = global.Promise;

/**
 * * Подключение уведомлений
 */

app.use(flash());

/**
 * * Глобальные переменные
 */

app.use(function(req, res, next) {  
  res.locals.user = req.user || null;
  res.locals.error_msg = req.flash("error_msg");
  next();
});

/**
 * * Подключение парсера
 */

app.use(bodyParser.urlencoded({limit: '50mb', extended: false }))
app.use(bodyParser.json({limit: '50mb'}));

/**
 * * Method override middleware
 */

app.use(methodOverride('_method'));


/**
 * * Подключение handlebars middleware
 */

const { parseBodyPost,  formatDate }  = require('./helpers/hbs');

app.engine('handlebars', exphbs({ 
  helpers: {
    parseBodyPost,    
    formatDate
  },
  defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');

/**
 * * Подключение статики
 */

app.use(express.static(path.join(__dirname, 'public')));

/**
 * * Подключение роутов
 */

app.use('/', index);
app.use('/posts', posts);
app.use('/tasks', tasks);
app.use('/users', users);
app.use('/profile', profile);

http.listen(port, () => {
  console.log(`Запущен на ${port} порту`);
});