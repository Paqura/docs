const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const config = {
  mode: 'production',
  plugins: [
    new UglifyJsPlugin()
  ]
};

module.exports =  merge(common, config);