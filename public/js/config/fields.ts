export default [
  {
    required: true,
    name : 'title',
    placeholder : 'Название поста',
    tag : 'h1',
    class: 'cr-post__title',
    position: 0
  }, 
  {
    required: true,
    name : 'lang',
    placeholder : 'Язык программирования',
    tag : 'span',
    class: 'cr-post__lang',
    position: 1
  }
];