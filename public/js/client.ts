import '../css/index.scss';
import fields from './config/fields';
import Redactor from './classes/Redactor';
import Renderer from './classes/Renderer';
import OptionsController from './classes/Options/OptionsController';
import Menu from './classes/Menu';
import Chat from './classes/Chat';
import Tab from './classes/Tab/TabController';
import Popup from './classes/Popup';
import Category from './classes/Category';
import Cubegl from './classes/Cubegl/Cube';

document.addEventListener('DOMContentLoaded', () => {   
  const redactor = new Redactor(fields);  
  const chat = new Chat();
  const render = new Renderer('.single-post__inner');
  const render2 = new Renderer('.tasks');
  const menu = new Menu('.nav', '.nav-menu__link');   
  const options = new OptionsController();
  const tabs = new Tab('.profile__tabs');
  const popupScale = new Popup('.popup--scale', true);
  const category = new Category();
  const Cube = new Cubegl('cubegl');  
  
  const code = document.querySelectorAll('code');
  
  code.forEach(it => {
    hljs.highlightBlock(it)
  });
});

