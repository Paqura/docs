import axios from 'axios';

type Fields = {
  required?: boolean,
  name?: string,
  placeholder?: string,
  tag: string,
  class: string,
  position: number,
  text: string,
  path?: string
}
export default class Redactor {
  public defaultInputClass : string;
  private creator : HTMLElement;
  private preview : HTMLElement;
  private container : HTMLElement;
  public baseFields : Array < Fields >;
  public addRequiredFieldsButton : HTMLElement;
  public buttonsType : HTMLElement;
  public delegeteTypesButton : HTMLElement;
  public deleteFieldButton: HTMLElement;
  private counter : number;
  private form : HTMLFormElement;
  public finalString : string;
  public publishedButton : HTMLElement;

  constructor(fields) {
    this.creator = document.querySelector('.post-creater');    
    if(!this.creator) {
      return null;
    }
    this.preview = document.querySelector('.post-preview');
    this.finalString = '';
    this.form = document.querySelector('.post-creater__form');
    this.container = document.querySelector('.post-creater__form .form-inner');
    this.defaultInputClass = 'cr-input-field';
    this.addRequiredFieldsButton = document.querySelector('.js-add-field');
    this.buttonsType = document.querySelector('.buttons-type');
    this.delegeteTypesButton = document.querySelector('.js-types-list');
    this.counter = 0;
    this.baseFields = fields;

    this
      .addRequiredFieldsButton
      .addEventListener('click', this.addRequiredFields.bind(this));

    this
      .delegeteTypesButton
      .addEventListener('click', this.createBodyNode.bind(this))

    this.publishedButton = document.querySelector('.js-publish-that');

    this
      .publishedButton
      .addEventListener('click', this.sendFormToDB.bind(this));

    this
      .form
      .addEventListener('change', this.editTextField.bind(this));
  }

  deleteField(evt) {
    const parent = evt.target.closest('.input-field');
    const field = parent.querySelector('.cr-input-field');
    const fieldPosition = field.dataset.position;
    console.log(this.baseFields)
    this.baseFields.splice(fieldPosition, 1);
    parent.remove();
    this.fullRender();
    this.counter--;
  }

  /**
   * * Редактирование полей
   */

  editTextField(evt) {
    const targetFieldPosition = evt.target.dataset.position;
    
    this.baseFields[targetFieldPosition]['text'] = evt.target.value;
    this.fullRender();
  }

  /**
   * * Добавляет обязательные поля
   */

  addRequiredFields(evt : EventListener) {
    if (this.container.children.length) {
      const isEmpty = this.validateField(this.container.children[this.container.children.length - 1]);
      return !isEmpty
        ? this.showError('Поле обязательное, не оставляй его пустым')
        : (this.baseFields[this.counter - 1].text = isEmpty, this.createRequiredField())
    } else {
      this.createRequiredField();
    }
  }

  /**
   * * Создание враппера для обязательного поля
   */

  createRequiredFieldWrapper(flag: boolean) {
    const wrapper = document.createElement('div');

    if (flag) {
      const deleteButton = document.createElement('button');
      const icon = document.createElement('img');
      icon.setAttribute('src', '/img/delete-button.svg');
      icon.setAttribute('alt', 'Удалить поле');
      icon.setAttribute('width', '24');
      icon.setAttribute('height', '24');
      deleteButton.className = 'button button-delete-field js-delete-field';
      deleteButton.appendChild(icon);
      wrapper.appendChild(deleteButton);  
      deleteButton.addEventListener('click', this.deleteField.bind(this));    
    }
    wrapper.className = 'input-field post-creater__field';
    return wrapper;
  }

  /**
   * * Создание обязательного поля
   */

  createRequiredField() {
    this.removeError();

    if (this.counter >= this.baseFields.length) {
      return this.createBodyButtonsAndDeletePrevious();
    }

    const field = document.createElement('input');
    const wrapper = this.createRequiredFieldWrapper(false);
    field.setAttribute('name', this.baseFields[this.counter].name);
    field.setAttribute('placeholder', this.baseFields[this.counter].placeholder);
    field.setAttribute('data-position', this.baseFields[this.counter].position.toString());
    field.className = this.defaultInputClass;
    this.baseFields[this.counter].position = this.counter;

    wrapper.appendChild(field);
    this
      .container
      .appendChild(wrapper);
    field.focus();
    this.counter++;
  }

  /**
   * * Валидация поля
   */

  validateField(field) {
    let input = field.querySelector('input');
    let text = input
      .value
      .trim();
    return text.length
      ? text
      : false;
  }

  /**
   * * Заменяет стандартную кнопку
   * * добавления обязательного поля
   * * на список с выбором типов
   */

  createBodyButtonsAndDeletePrevious() {
    this
      .buttonsType
      .classList
      .add('js-more-types');
  }

  /**
   * * Абстрактная фабрика необязательных полей
   * * фабрика, в смысле, паттерн
   */

  createBodyNode(evt) {
    const nodeElement = evt.target;
    const nodeDataSet = evt.target.dataset.node;

    switch (nodeDataSet) {
      case 'h':
        return this.createNodeForPreview('h3', 'input', 'cr-post__header');
      case 'img':
        return this.createImageNodeForPreview(nodeElement);
      case 'p':
        return this.createNodeForPreview('p', 'textarea', 'cr-post__paragraph');
      case 'code':
        return this.createNodeForPreview('code', 'textarea', 'cr-post__code');
      default:
        break;
    }
  }

  /**
   * * Перевод нод в данные
   * * и транспорт в отрисовку инпутов под них
   */

  createImageNodeForPreview(node : HTMLElement) {
    let path = {
      url: null
    };

    node
      .querySelector('input')
      .addEventListener('change', evt => {
        const self = this;
        const input = evt.target;
        const fileReader = new FileReader();
        fileReader.onload = function () {
          path.url = fileReader.result;
          self.createNodeForPreview('img', 'img', 'cr-post__image', path.url);
        }
        fileReader.readAsDataURL(input.files[0]);
        input.value = '';
      });
  }

  createNodeForPreview(tag : string, tagForWriteText : string, className : string, path : string = null) {
    this.counter++;
    this
      .baseFields
      .push({
        tag,
        text: '',
        class: className,
        position: this.counter - 1,
        path
      })
    this.appendFieldsToForm(tagForWriteText, this.baseFields[this.counter - 1]['class'], tag, path);
  }

  /**
   * * Добавление нод в форму
   * * и прослушивание события инпут
   */

  appendFieldsToForm(tagForWriteText : string, className : string, tag, path : string = null) {
    const tagForWrite = document.createElement(tagForWriteText);
    const wrapper = this.createRequiredFieldWrapper(true);

    tagForWrite.className = this.defaultInputClass + ' ' + this.baseFields[this.counter - 1]['class'];
    tagForWrite.dataset.position = this
      .baseFields[this.counter - 1]['position']
      .toString();
    wrapper.appendChild(tagForWrite);
    this
      .container
      .appendChild(wrapper);
    tagForWrite.focus();

    tagForWrite.addEventListener('input', (evt : KeyboardEvent) => {
      evt.target.style.height = evt.target.scrollHeight + 'px';
      if (tag === 'code') {
        let codeText = evt.target.value;
        this.baseFields[this.counter - 1]['text'] = evt.target.value;
      } else {
        this.baseFields[this.counter - 1]['text'] = evt.target.value;
      }
    })
    this.fullRender();
  }

  /**
   * * Отрисовка полного превью
   */

  fullRender() {
    const container = this
      .preview
      .querySelector('main');
    container.innerHTML = '';
    this
      .baseFields
      .map(it => {
        const node = document.createElement(it['tag']);
        node.textContent = it['text'];
        node.className = it['class'];
        /**
         * * Если поле 'path' не пустое
         * * то создается враппер
         * * и в него помещается картинка
         */
        if (it['path']) {
          const imageWrapper = document.createElement('span');
          imageWrapper.className = 'cr-post__image-wrapper';
          node.setAttribute('src', it['path']);
          imageWrapper.appendChild(node);
          container.appendChild(imageWrapper);
          this.finalString = container.innerHTML;
        } else {
          container.appendChild(node);
          this.finalString = container.innerHTML;
        }
      })
  }

  /**
   * * Отправка формы
   */

  sendFormToDB() {
    const textarea = document.createElement('textarea');
    const submit : HTMLFormElement = document.querySelector('.js-create-post-submit');
    textarea.setAttribute('name', 'body');
    textarea.innerText = JSON.stringify(this.finalString);

    this
      .container
      .appendChild(textarea);
    this.form.onsubmit = null;
    submit.click();
  }

  /**
   * * Методы ошибок
   */

  showError(text) {
    const error = document.createElement('span');
    const prepublish = document.querySelector('.pre-publish');
    error.className = 'error-field';
    error.textContent = text;
    prepublish.appendChild(error);
  }

  removeError() {
    const isExist = this.checkErrorField();
    if (isExist) {
      isExist.remove()
    }
  }

  checkErrorField() {
    let error = document.querySelector('.error-field');
    return error;
  }

}
