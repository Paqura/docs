import TabView from './TabView';
import TabModel from './TabModel';

export default class TabController {
  protected model;
  protected view;
  
  constructor(className: string) {
    this.model = new TabModel();
    this.view = new TabView(className, this.model);

    this.view.on('changeTabView', this.addChangeTabView);
  }

  addChangeTabView = (evt: MouseEvent, links: NodeList) => {
    const state = this.model.changeState(evt, links);
    return this.view.changeTabView(state);
  };
}