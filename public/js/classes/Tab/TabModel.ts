type TabState = {
  activeTab: string | number;
  length: string;
};

export default class TabModel {
  public tabState: TabState;  

  constructor() {
    this.tabState = {
      activeTab: null,
      length: null
    };
  }

  changeState = (evt: MouseEvent, links: NodeList) => {
    const tabDataset = evt.target['dataset'].tabHref;
    const length = links.length.toString();

    this.tabState = {
      activeTab: tabDataset,
      length
    };
    
    return this.tabState;
  }
}