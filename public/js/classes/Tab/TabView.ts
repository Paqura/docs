import EventEmmiter from '../EventEmmiter';

export default class TabView extends EventEmmiter{
  public tabNodes;
  public tabLinks: NodeList;
  public currentTab: string | number;
  public tabContent;
  
  constructor(className: string, model: Object) {
    super();
    this.tabNodes = document.querySelector(className);  

    if(this.tabNodes) {
      this.tabLinks = this.tabNodes.querySelectorAll('a');
      this.currentTab = 'info';
      this.tabContent = document.querySelectorAll('.tab-content');
      this.setTabContent();

      this.tabNodes.addEventListener('click', evt => {         
        if(evt.target.classList.contains('profile__tabs-link')) {
          evt.preventDefault();
          this.emit('changeTabView', evt, this.tabLinks);
        }
      });
    }
  }

  setTabContent = () => {
    this.tabContent.forEach(it => {
      if(it.dataset.tab !== this.currentTab) {
        it.classList.remove('is-show');
      } else {
        it.classList.add('is-show');
      }
    });
  }

  changeTabView({ activeTab }) {
    this.currentTab = activeTab;

    this.addClassForTabs();
    this.setTabContent();
  }

  addClassForTabs() {
    this.tabLinks.forEach((it: HTMLElement) => {
      if(!(it.dataset.tabHref === this.currentTab)) {
        it.classList.remove('is-active');
      } else {
        it.classList.add('is-active');
      }
    });
  }
}