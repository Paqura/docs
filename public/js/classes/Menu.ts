import debounce from '../helpers/debounce';
export default class Menu {
  private links : NodeList;
  private path : string;
  private nav : HTMLElement;
  private showMobile : HTMLElement;
  private hideMobile : HTMLElement;
  private mobileMenu : HTMLElement;
  private location : string;
  private goBack : HTMLElement;

  constructor(node : string, linksClass : string) {
    this.nav = document.querySelector(node);
    this.showMobile = document.querySelector('.js-show-mobile-menu');
    this.hideMobile = document.querySelector('.js-hide-mobile-menu');
    this.mobileMenu = document.querySelector('.menu-mobile');
    this.links = document.querySelectorAll(linksClass);
    this.path = window.location.pathname;
    this.goBack = document.querySelector('.go-back');

    this.acceptCurrentHref = this.acceptCurrentHref.bind(this);
    this.acceptCurrentHref();

    this.showMobile.addEventListener('click', this.toggleMobileMenu.bind(this));
    this.hideMobile.addEventListener('click', this.toggleMobileMenu.bind(this));    

    this.goBack.addEventListener('click', this.goHistoryBack.bind(this));

    document.addEventListener('wheel', debounce(this.setPosition.bind(this), 20));
    
    this.checkLocation();
  }

  getLinks() : NodeList {
    return this.links;
  }

  getHref() : string {
    return this.path;
  }

  goHistoryBack() {
    window.history.back();
  }

  checkLocation() {
    if(this.getHref() === '/') {
      this.nav.classList.add('is-transparent');
    }
    if (this.getHref() === '/users/login' || this.getHref() === '/users/register') {
      this.goBack.classList.add('is-show');
      this.nav.remove();
    }
    if(window.innerWidth < 500) {
      this.goBack.remove();
    }
  }

  acceptCurrentHref() {
    const linksList = Array.from(this.getLinks());
    const href : string = this.getHref();

    linksList.forEach((link : HTMLElement) => {    
      const target : string = link.dataset.target;
      if (target === href) {
        link.classList.add('--is-active');
      } else {
        link.classList.remove('--is-active');
      }
    });
  }

  setPosition(evt) : void {
    const deltaY = evt.deltaY;
    if (deltaY < 0) {
      this.nav.classList.add('is-sticky');
    } else {
      this.nav.classList.remove('is-sticky');
    }
  }

  toggleMobileMenu(evt) {
    evt.preventDefault();
    if (this.mobileMenu.classList.contains('is-open')) {
      this.mobileMenu.classList.remove('is-open');
      this.setPosition = null;
      document.body.style.position = 'static';
    } else {
      this.mobileMenu.classList.add('is-open');
      document.body.style.position = 'fixed';
    }
  }
}