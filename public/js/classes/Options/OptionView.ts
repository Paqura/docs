import EventEmmiter from '../EventEmmiter';

export default class OptionsView extends EventEmmiter{
  public optionsHTML: HTMLElement;
  public optionsList: HTMLElement; 
  public finderModal: HTMLElement; 
  public closefinderModalButton: HTMLElement;
  public toggler: HTMLElement;
  public finderButton: HTMLElement;
  public chatToggler: HTMLElement;
  public chat;
  public closerChat;
  public model;
  public updateState;
  public addChangeOptionsState;

  constructor(model) {   
    super(); 
    this.model = model;
    this.optionsHTML = document.querySelector('.buttons-options');    
    this.optionsList = document.querySelector('.buttons-options__list');     
    this.toggler = document.querySelector('.js-options');
    this.chat = document.querySelector('.chat');
    
    if(this.toggler) {
      this.finderButton = document.querySelector('.js-finder'); 
      this.chatToggler = document.querySelector('.js-chat'); 
      this.finderModal = document.querySelector('.js-modal-search'); 
      this.closefinderModalButton = document.querySelector('.js-search-close'); 
      this.closerChat = document.querySelector('.js-close-chat');

      this.closerChat.addEventListener('click', () => {
        this.emit('changeChatState', null);
      });
  
      this.toggler.addEventListener('click', this.findOptionsButton);   
      
      this.chatToggler.addEventListener('click', () => {
        this.emit('changeChatState', null);
      }); 
      
      this.finderButton.addEventListener('click', () => {
        this.emit('changeFinderState', null);
      });    
      this.closefinderModalButton.addEventListener('click', () => {
        this.emit('changeFinderState', null);
      })
    }   
  }

  findOptionsButton = (evt) => {
    const currentTarget = evt.target.closest('button');
    this.emit('changeStateOptions', currentTarget);
  }  

  changeState = (state)  => {
    if(state) {
      this.optionsList.classList.add('is-open');
    } else {
      this.optionsList.classList.remove('is-open');
    }
  }

  changeFinderState = (state)  => {
    if(state) {
      this.finderModal.classList.add('is-open');
      this.finderModal.querySelector('input').focus();
    } else {
      this.finderModal.classList.remove('is-open');
    }
  }
  changeChatState = (state)  => {
    if(state) {
      this.chat.classList.add('is-open');
      this.chat.querySelector('input').focus();
    } else {
      this.chat.classList.remove('is-open');
    }
  }
}