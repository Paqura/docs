import OptionsView from './OptionView';
import OptionsModel from './OptionsModel';
export default class OptionsController {
  public view: OptionsView;
  public model: OptionsModel;

  constructor() {    
    this.model = new OptionsModel();
    this.view = new OptionsView(this.model);
    
    this.view.on('changeStateOptions', this.addChangeState.bind(this));   
    this.view.on('changeFinderState', this.addChangeFinderState.bind(this));   
    this.view.on('changeChatState', this.addChangeChatState.bind(this));   
  } 

  addChangeState = (button) => {    
    const state = this.model.changeModelState();    
    return this.view.changeState(state);
  }  

  addChangeFinderState = (arg) => {
    const state =  this.model.changeFinderState();
    return this.view.changeFinderState(state);
  }
  addChangeChatState = (arg) => {
    const state =  this.model.changeChatState();
    return this.view.changeChatState(state);
  }
}