export default class OptionsController {
  public data;
  constructor() {
    this.data = {
      state: false,
      finder: false,
      chat: false
    };    
  }  

  changeModelState = () => {
    const newState = !this.data.state;
    this.data.state = newState; 
    return newState;     
  }

  changeFinderState = () => {
    const newState = !this.data.finder;
    this.data.finder = newState; 
    return newState;     
  }
  changeChatState = () => {
    const newState = !this.data.chat;
    this.data.chat = newState; 
    return newState;     
  }
}