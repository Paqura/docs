export default class Renderer {
  private text: string;
  private nodes: NodeList;
  private loader: HTMLElement;
  private posts: HTMLElement;

  constructor(node: string) {
    this.loader = document.querySelector('.loader');
    this.nodes = document.querySelectorAll(node);
    this.text = '';
    if(this.nodes) {
      this.createAndPasteHTML();
    }    
  }

  createAndPasteHTML(): void {
    this.nodes.forEach((it: HTMLElement) => {
      it.innerHTML = it.textContent;   
      this.showContent(it.closest('.post-content'));
      const code = document.querySelectorAll('code');
  
      code.forEach(it => {
        hljs.highlightBlock(it)
      });
    });
  } 
  
  showContent(el) {
    this.loader.style.transform = 'rotateZ(0deg) scale(0)';
    el.style.opacity = '1'; 
  }  
}