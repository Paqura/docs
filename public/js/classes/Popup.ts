export default class Popup {
  public state: boolean;
  public popup: HTMLElement;
  public closer: HTMLElement;

  constructor(popupName: string, state: boolean) {
    this.popup = document.querySelector(popupName);
    if(this.popup) {
      this.closer = document.querySelector('.js-close-popup');
      this.state = state;
      
      this.closer.addEventListener('click', this.toggleState);
    }
  }

  getState(): boolean {
    return this.state;
  }

  toggleState = (): void =>  {
    this.state ? this.state = false : this.state;
    this.changeView();
  }

  changeView = () => {
    if(this.state) {
      this.popup.classList.remove('is-hidden');
    } else {
      this.popup.classList.add('is-hidden');
    }
  }
}