export default class Chat {
  public socket;
  public chatBox;
  public form;
  public input;
  public container;
  public userName;
  
  constructor() {
    this.form = document.querySelector('.chat-form');
    if(this.form) {
      this.socket = io();     
      this.chatBox = document.querySelector('.chat');
      this.container = document.querySelector('.chat__output');   
      this.input = this.form.querySelector('input');

      this.socket.on('sendMessage', this.renderMessages.bind(this));
      this.form.addEventListener('submit', this.sendMessage.bind(this));
    }   
  }

  sendMessage = (evt) => {
    evt.preventDefault();
    const message = this.input.value;    
    this.socket.emit('sendMessage', message);
    this.input.value = '';
    return false;
  }

  renderMessages = (message: string, own: boolean) => {
    const mess = document.createElement('li');
    mess.className = 'chat__message chat__message--new';
    mess.textContent = own ? 'Вы: ' + message.toString() : message.toString();
    this.container.appendChild(mess);  
  }
}