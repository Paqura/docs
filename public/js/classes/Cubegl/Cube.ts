import * as THREE from 'three';
const OrbitControls = require('three-orbit-controls')(THREE);
import vertex from './vertex.glsl';
import fragment from './fragment.glsl';

export default class Cube {
  /**
   * * {space} Размеры квадрата, в котором
   * * генерируются частички
   * * {particlesLength} кол-во частичек
   * * {positions} координаты во flat массиве
   * * {points} массив частичек
   * * {pointsData} массив скоростей частичек
   */

  public container;
  public camera;
  public controls;
  public scene;
  public renderer;
  public pointsGeometry;
  public pointsMaterial;
  public plane;
  public timer;
  public fragment;
  public vertex;
  public points;
  public lineMaterial;
  public linesGeometry;
  public lineMesh;
  public pointsData;
  public particlesLength: number;
  public space: number;
  public positions: Float32Array;
  public linePositions: Float32Array;

  constructor(containerName: string) {
    this.container = document.getElementById(containerName);
    if(this.container) {
      this.timer = 0;
      this.pointsData = [];
      this.particlesLength = 1000;
      this.positions = null;
      this.space = 800;
      this.fragment = fragment;
      this.vertex = vertex;
      this.scene = new THREE.Scene();
      this.renderer = new THREE.WebGLRenderer();
      this.renderer.setPixelRatio(window.devicePixelRatio);
      this.renderer.setSize(window.innerWidth, window.innerWidth);
    
      this.appendCanvas();        
      this.controls = new OrbitControls(this.camera, this.renderer.domElement);  
      this.animate();
    }
  }

  appendCanvas = (): void => {
    this.container.appendChild(this.renderer.domElement);
    this.setCameraSettings();
    this.setMaterialSettings();  
  }

  setCameraSettings = (): void => {
    this.camera = new THREE.PerspectiveCamera(
      70,
      window.innerWidth / window.innerHeight,
      0.001, 3000
    );
  
    this.camera.position.set( 0, 0, 500);
  }

  setMaterialSettings = () => {
    this.pointsMaterial = new THREE.PointsMaterial({
      color: 0xffffff,
      size: 2
    });

    this.lineMaterial = new THREE.LineBasicMaterial({
      color: 0xffffff
    });    

    this.positions = new Float32Array(this.particlesLength * 3);
    this.linePositions = new Float32Array(this.particlesLength * 3 * 10);

    for(let i = 0; i < this.particlesLength; i++) {
      const x = (Math.random() - 0.5) * this.space;
      const y = (Math.random() - 0.5) * this.space;
      const z = (Math.random() - 0.5) * this.space;

      this.positions[3 * i] = x;
      this.positions[3 * i + 1] = y;
      this.positions[3 * i + 2] = z;

      this.pointsData.push({
        velocity: new THREE.Vector3(Math.random(), Math.random(), Math.random())
      })
    }

    this.pointsGeometry = new THREE.BufferGeometry();
    this.linesGeometry = new THREE.BufferGeometry();

    this.pointsGeometry.addAttribute('position', new THREE.BufferAttribute(this.positions, 3).setDynamic(true));
    this.linesGeometry.addAttribute('position', new THREE.BufferAttribute(this.linePositions, 3).setDynamic(true));
  
    this.lineMesh = new THREE.LineSegments(this.linesGeometry, this.lineMaterial);

    this.points = new THREE.Points(this.pointsGeometry, this.pointsMaterial);

    this.scene.add(this.points);
    this.scene.add(this.lineMesh);
    this.resize();
  }

  animate = () => {    
    this.timer = this.timer + 0.05;

    this.setVelocity();

    requestAnimationFrame(this.animate);
    this.render();
  }

  render = () => {        
    this.resize();
    this.camera.position.set( 0, 0, 500 - this.timer + 1);
    this.renderer.render(this.scene, this.camera);
  }

  /**
   * * Функция задаёт скорость каждой частичке,
   * * и, если скорость больше пространства-контейнера (space),
   * * задаёт отрицательную скорость (частичка отскакивает)
   */

  setVelocity = (): void => {
    let vertexPosition = 0;
    let connections = 0;

    for (let i = 0; i < this.particlesLength; i++) { 

      this.positions[3 * i] += this.pointsData[i].velocity.x;
      this.positions[3 * i + 1] += this.pointsData[i].velocity.y;
      this.positions[3 * i + 2] += this.pointsData[i].velocity.z;

      if(this.positions[3 * i] > this.space/2 || this.positions[3 * i] < -this.space/2) { 
        this.pointsData[i].velocity.x = -this.pointsData[i].velocity.x;
      }

      if(this.positions[3 * i + 1] > this.space/2 || this.positions[3 * i + 1] < -this.space/2) { 
        this.pointsData[i].velocity.y = -this.pointsData[i].velocity.y;
      }

      if(this.positions[3 * i + 2] > this.space/2 || this.positions[3 * i + 2] < -this.space/2) { 
        this.pointsData[i].velocity.z = -this.pointsData[i].velocity.z;
      }

      for(let j = i + 1; j < this.particlesLength; j++) {
        const dx = this.positions[i * 3] - this.positions[j * 3];
        const dy = this.positions[i * 3 + 1] - this.positions[j * 3 + 1];
        const dz = this.positions[i * 3 + 2] - this.positions[j * 3 + 2];
        
        const distance = Math.sqrt(dx*dx + dy*dy + dz*dz);

        if(distance < 20) {
          this.linePositions[vertexPosition++] = this.positions[i * 3];
          this.linePositions[vertexPosition++] = this.positions[i * 3 + 1];
          this.linePositions[vertexPosition++] = this.positions[i * 3 + 2];

          this.linePositions[vertexPosition++] = this.positions[j * 3];
          this.linePositions[vertexPosition++] = this.positions[j * 3 + 1];
          this.linePositions[vertexPosition++] = this.positions[j * 3 + 2];         
          
          connections++;
        }
      }

      this.lineMesh.geometry.setDrawRange(0, connections * 2);
      this.points.geometry.attributes.position.needsUpdate = true;
      this.lineMesh.geometry.attributes.position.needsUpdate = true;
    }
  }  

  resize = () => {
    const w = window.innerWidth;
    const h = window.innerHeight;
    this.renderer.setSize( w, h );
    this.camera.aspect = w / h;    
    this.camera.updateProjectionMatrix();
  }
}