export default class Category {
  private path;
  public links;
  public selected;
  public state;

  constructor() {
    this.path = window.location.pathname;
    this.selected = document.querySelector('.category-select');
    this.links = document.querySelectorAll('.category__link');
  
    if(this.links) {
      this.setActive(this.path);
    }
    if(this.selected) {
      this.state = [];
      Array.from(this.selected.options).forEach((it, i) => this.state.push({[i] : it['value']}));
      this.changeOptionValue();
      this.selected.addEventListener('change', this.setActive);
    }
  }

  
  setActive = (evt) => {
    if(evt.type === 'change') {
      window.location.pathname = evt.target.value;      
    }  
    this.links.forEach(it => {
      if(it.dataset.href === this.path) {
        it.classList.add('is-active');
      } else {
        it.classList.remove('is-active');
      }
    });    
  }

  changeOptionValue = () => {
    const current = this.state.map((it, i) => it[i] === this.path);    
    let count;
    current.forEach((it, i) => it ? count = i : count);    
    this.selected.options.selectedIndex = count;
  }
}