export default class EventEmmiter {
  public events;

  constructor() {
    this.events = {};
  }

  on(type, cb) {
    this.events[type] = this.events[type] || [];
    this.events[type].push(cb);
  }
  emit(type, ...args) {
    if(this.events[type]) {
      this.events[type].forEach(cb => cb(...args))
    }
  }
}