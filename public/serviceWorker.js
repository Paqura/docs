const currentCacheName = 'part-sw-1';

const filesToCache = [
  './js/client.js',
  './img/logo.svg',
  './img/kite.svg',
  './img/menu.svg',
  './video/blog.mp4'
];

/**
 * * Засовываем все наши прекрасные ассеты в кеш
 */

self.addEventListener('install', (evt) => {
  evt.waitUntil(
    caches.open(currentCacheName)
      .then(cache => cache.addAll(filesToCache))
      .catch(err => console.log(err))
  )
})

/**
 * * Чистим кеш от старого
 */

self.addEventListener('activate', (evt) => {
  evt.waitUntil(
    caches.keys()
      .then((cacheNames) => {
        return Promise.all(
          cacheNames
            .filter((cacheName) => cacheName !== currentCacheName)         
            .map(cacheName => caches.delete(cacheName))           
        )
    })
  )
})

/**
 * * Фетчим данные
 */

self.addEventListener('fetch', evt => {
  evt.respondWith(
    caches.match(evt.request)
      .then(response => {
        return response || fetch(evt.request)
          .then(resp => {
            return caches.open(currentCacheName)
              .then(cache => {
                cache.put(evt.request, resp.clone());
                return resp;
              })
          })
      })
  )
});